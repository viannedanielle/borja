Guidelines:
	1. Remember to reference css and javascript files properly. Ensure that files being referenced are in the correct directory.
	2. Identify which template was used for the page. Download the template through the bootstrap website (getbootstrap.com).
	3. Contents for the web page are already written below. Identify where in the Twitter bootstrap template the content will be put.
	4. This exercise is open notes. You may use any resources (except your classmates) that may help you with the web page.

==========================================================================================

PAGE TITLE: NBA News
PROJECT NAME: NBA News

NAVBAR
Dropdown
	Stats
		=> Pre-season
		=> Season
		=> Playoffs

CAROUSEL
First Slide
	Image Filename: kawhi.jpg
	Headline: Kawhi Leonard out for the playoffs.
	Paragraph: He was never expected to play and was previously considered doubtful. With the Spurs trailing the Warriors 3-0, this could be the end of Leonard's season. Expect to see a lot of Jonathon Simmons tonight with Kyle Anderson picking up some extra minutes as well.
Second Slide
	Image Filename: gsw.jpg
	Headline: 2017 NBA champions
	Pargaraph: The Golden State Warriors losing the 2016 NBA title to the Cleveland Cavaliers after taking a 3-1 series lead haunted Steph Curry, Draymond Green, Klay Thompson, coach Steve Kerr and the rest of the team from the front office down. The decision was made that they had to do something drastic, that they wouldn't lose again.
Third Slide
	Image Filename: rumor.jpg
	Headline: Lebron James to Los Angeles in 2018 free agency
	Paragraph: LeBron James and the Cavaliers are still battling the Warriors in the NBA Finals. But James's future in Cleveland appears uncertain. Kevin O'Connor reports that "rumblings across the league" indicate James could be interested in a move to Los Angeles, to play for either the Clippers or Lakers.

TOP PLAYERS
Header: NBA 2016-2017 Season Top Players
First Player
	Header: Lebron James 23
	Image Filename: lebron.jpg
	Paragraph: LeBron Raymone James is an American professional basketball player for the Cleveland Cavaliers of the National Basketball Association.
Second Player
	Header: JaVale Lindy McGee
	Image Filename: mcgee.jpg
	Paragraph: JaVale Lindy McGee is an American professional basketball player for the Golden State Warriors of the National Basketball Association. He was selected 18th overall by the Washington Wizards in the 2008 NBA draft.
Third Player
	Header: Kevin Durant 35
	Image Filanme: durant.jpg
	Paragraph: Kevin Wayne Durant is an American professional basketball player for the Golden State Warriors of the National Basketball Association.

FEATURETTES
First Featurette
	Header: Kia NBA Most Valuable Player
	Image Filename: westbrook.jpg
	Paragraph: Russell Westbrook delivered quite possibly the most impressive personal season in 2016-17 on NBA record. Averaging a triple-double for the first time in over 50-years, and only the second player (Oscar Robertson) to ever accomplish this feat. What made this accomplishment even more impressive is the copious dynamics surrounding Westbrook’s situation.
Second Featurette
	Header: Kia NBA Rookie of the Year
	Image Filename: brogdon.jpg
	Paragraph: Malcolm Brogdon of the Milwaukee Bucks is the best choice for Kia Rookie of the Year as the most consistent first-year player among the three options. He’s the only one on a playoff team with his dependability a necessity rather than a preference, and he’s been a symbol of efficiency during an underwhelming 2016-17 filled with inefficiency by the rookie class.
Third Featurette
	Header: Kia NBA Flopper of the Year
	Image Filename: snart.jpg
	Paragraph: Down by three with 16 seconds left in regulation, Smart got the refs to call a flagrant foul on James Harden after Harden tried to clear some space with an elbow. Smart knocked down the two free-throws and the Celtics had a chance to win the game but Al Horford missed an easy game-winning layup attempt.

